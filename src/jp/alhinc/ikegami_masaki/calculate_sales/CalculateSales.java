package jp.alhinc.ikegami_masaki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {


	public static void main(String[] args) {
		HashMap<String, String> BranchName = new HashMap<String, String>();
		HashMap<String, Long> BranchSales = new HashMap<String, Long>();

//		引数が２つ以上は処理を終了
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

//		branch.lstを読み込む処理
		if(readFile(args[0], "branch.lst", "支店", "[0-9]{3}", BranchName, BranchSales) == false){
			return;
		}

//		売上を集計
		File dir = new File(args[0]);
		String[] list = dir.list();
		ArrayList<String> fileList = new ArrayList<String>();
//		売上ファイルの抽出
		for(int i=0; i<list.length; i++){
			String fileName = list[i];
			File file = new File(args[0], fileName);
			if(fileName.matches("[0-9]{8}.rcd") &&  file.isFile() ){
				fileList.add(fileName);
			} else if(file.isFile() && (fileName.matches("[0-9]{8}.rcd.*") || fileName.matches(".*[0-9]{8}.rcd"))){
				//ファイル名前後のごみチェック
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

//		売上ファイルの連番確認
		for(int i=0; i<fileList.size(); i++){
			String fileName = fileList.get(i);
//			連番になっていない時はメッセージ
			if( !(fileName.contains( (i+1) + ".rcd")) && fileName.matches("[0-9]{8}.rcd") ){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		for(String name : fileList){
//			別メソッドにて記載
//			ファイルを開く
			BufferedReader fileBr = null;
			try {
//				ファイルをバッファーで開く
				fileBr = useBfReader(args[0], name);

//				支店id、売上を取得
				String branchId = fileBr.readLine();
				String priceStr = fileBr.readLine();
				Long price;

//				行数がおかしいとき
				if(priceStr == null || branchId == null || fileBr.readLine() != null) {
					System.out.println(name + "のフォーマットが不正です");
					return;
				}

//				支店コードがおかしい場合
				if(BranchName.containsKey(branchId) == false){
					System.out.println(name + "の支店コードが不正です");
					return;
				}

				price  = Long.parseLong(priceStr);
//				売上の合計処理
				price += BranchSales.get(branchId);

//				String型に変換して合計金額のチェック
//				合計１０桁以上
				if(String.valueOf(price).matches("[0-9]{11,}")){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

//				売上をセット
				BranchSales.put(branchId, price);

			} catch(Exception e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(fileBr != null){
					try {
						fileBr.close();
					} catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

//		ファイルの書き込み処理
		if(writeFile(args[0], "branch.out", BranchName, BranchSales) == false) {
			return;
		}
	}

//	リーダーを返す。失敗したらnull
	static BufferedReader useBfReader(String dirName, String fileName) {
		BufferedReader br = null;

		try{
//		ファイルをバッファーで開く
		File file = new File(dirName, fileName);
//		ファイルが存在しない場合
		if(!file.exists()){
			return null;
		}
		FileReader fileReader = new FileReader(file);
		br = new BufferedReader(fileReader);
		} catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
			return null;
		}

		return br;
	}

//	branch.lstの読み込み処理
//	引数checkは1行目の正規表現
	static boolean readFile(String dirName, String fileName, String message, String check,
							HashMap<String, String> hashName, HashMap<String, Long> hashSales){
//		支店定義ファイル
		BufferedReader br = null;
		try {
//			ファイルをバッファーで開く
			if((br = useBfReader(dirName, fileName)) == null){
				System.out.println(message + "定義ファイルが存在しません");
				return false;
			}

//			情報をセットする
			String line;
			while((line = br.readLine()) != null){
				String[] info = line.split(",");
//				フォーマットの確認
//				カンマが１つ、支店番号が三桁
				if(info[0].matches(check) && info.length == 2 ){
					String branchKey = info[0];
					String branchValue = info[1];

					hashName.put(branchKey, branchValue);
					hashSales.put(branchKey, 0L);
				} else {
					System.out.println(message + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		} catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null){
				try {
					br.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
//		処理の成功
		return true;
	}

//	支店別集計ファイルの出力
	static boolean writeFile(String dirName, String fileName,
							 HashMap<String, String> hashName, HashMap<String, Long> hashSales){
		BufferedWriter bw = null;
		try{
//			ファイルの取得
			File file = new File(dirName, fileName);
			FileWriter writeFile = new FileWriter(file);
			bw = new BufferedWriter(writeFile);

//			書き込み処理
			for(String shopId : hashName.keySet()){
				String shopName = hashName.get(shopId);
				Long shopSales = hashSales.get(shopId);

				bw.write(shopId + "," + shopName + "," + shopSales);
				bw.newLine();
			}
		} catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null){
				try {
					bw.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
//		処理の成功
		return true;
	}
}

